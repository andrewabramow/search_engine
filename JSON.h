#pragma once

#include <iostream>
#include <vector>
#include "Server.h"


class ConverterJSON {
public:
	ConverterJSON() = default;
	/**
	* ����� ��������� ����������� ������
	* @return ���������� ������ � ���������� ������ �������������
	*       � config.json
	*/
	std::vector<std::string> GetTextDocuments();

	/* ����� ��������� ���� max_responses ��� ����������� �����������
	*  ���������� ������� �� ���� ������
	* @return
	*/
	int GetResponsesLimit();
	/*
	* ����� ��������� �������� �� ����� requests.json
	* @return ���������� ������ �������� �� ����� requests.json
	*/
	std::vector<std::string> GetRequests();
	/*
	* �������� � ���� answers.json ���������� ��������� ��������
	*/
	void putAnswers(std::vector<std::vector<std::pair<int, float>>>);
};

std::string RequestNumber(int n);

double ToTwoDecimalPlaces(float f);