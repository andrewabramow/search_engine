﻿// search_engine.cpp : Defines the entry point for the application.
//

#include <iostream>
#include <string>
#include <vector>
#include "gtest/gtest.h"
#include "search_engine.h"
#include "JSON.h"
#include "InvertIndex.h"
#include "Server.h"

#ifdef TESTS 
int main()
{
	//  JSON part:
	ConverterJSON newJSON;
	//  paths to documents
	//for (auto& el : newJSON.GetTextDocuments()) {
	//	std::cout << el << std::endl;
	//}
	//  list of requests
	for (auto& el : newJSON.GetRequests()) {
		std::cout << el << std::endl;
	}
	//  response limit
	std::cout << "Response limit is: " <<
		newJSON.GetResponsesLimit() << std::endl;
//________________________________________________________________

	//  Inverted index part:
	//  (pointer for SearchServer constructor
	InvertedIndex* newInvInd = new InvertedIndex();
	//  update documents
	newInvInd->UpdateDocumentBase(newJSON.GetTextDocuments());
	////  entries of certain word
	//std::cout << "Entries of 'milk':\n";
	//for (auto& el : newInvInd->GetWordCount("milk")) {
	//	std::cout << el.doc_id << " : " << el.count << std::endl;
	//}
//________________________________________________________________

	//  Search server part:
	SearchServer* server = new SearchServer(*newInvInd/*, newJSON.GetResponsesLimit()*/);
	auto requests = newJSON.GetRequests();
	auto answers = ConvertAnswer(server->search(requests));
	//for (auto& El : answers) {
	//	for (auto& el : El) {
	//		std::cout << el.first << " : "<<el.second<<std::endl;
	//	}
	//	std::cout << std::endl;
	//}
	newJSON.putAnswers(answers);
//________________________________________________________________

	//  Memory clear:
	delete newInvInd;

	//return RUN_ALL_TESTS();
}
#endif
