#include <vector>
#include <map>
#include <algorithm>
#include <unordered_set>
#include "InvertIndex.h"
#include "Server.h"

InvertedIndex SearchServer::GetInvertedIndex() {
	return _index;
}
size_t MaxCount(std::vector<Entry> freq) {
	size_t max_count{ 0 };
	for (auto& el : freq) {
		if (el.count > max_count) max_count = el.count;
	}
	return max_count;
}
void MergeRI(std::vector<RelativeIndex>& input,
	int const left,
	int const mid,
	int const right)
	// Merges two subvectors of input.
	// First vector is input[begin..mid]
	// Second vector is input[mid+1..end]
{
	auto const subVecOne = mid - left + 1;
	auto const subVecTwo = right - mid;

	// Create temp vectors
	std::vector <RelativeIndex> leftVec;
	std::vector <RelativeIndex> rightVec;

	// Copy data to temp vectors leftVec[] and rightVec[]
	for (auto i = 0; i < subVecOne; i++)
		leftVec.push_back(input[left + i]);
	for (auto j = 0; j < subVecTwo; j++)
		rightVec.push_back(input[mid + 1 + j]);

	auto indexOfSubVecOne = 0, // Initial index of first sub-vector
		indexOfSubVecTwo = 0; // Initial index of second sub-vector
	int indexOfMergedVec = left; // Initial index of merged vector

	// Merge the temp vectors back into input[left..right]
	while (indexOfSubVecOne < subVecOne && indexOfSubVecTwo < subVecTwo) {
		if (leftVec[indexOfSubVecOne].rank >= rightVec[indexOfSubVecTwo].rank) {
			input[indexOfMergedVec] = leftVec[indexOfSubVecOne];
			indexOfSubVecOne++;
		}
		else {
			input[indexOfMergedVec] = rightVec[indexOfSubVecTwo];
			indexOfSubVecTwo++;
		}
		indexOfMergedVec++;
	}
	// Copy the remaining elements of
	// left[], if there are any
	while (indexOfSubVecOne < subVecOne) {
		input[indexOfMergedVec] = leftVec[indexOfSubVecOne];
		indexOfSubVecOne++;
		indexOfMergedVec++;
	}
	// Copy the remaining elements of
	// right[], if there are any
	while (indexOfSubVecTwo < subVecTwo) {
		input[indexOfMergedVec] = rightVec[indexOfSubVecTwo];
		indexOfSubVecTwo++;
		indexOfMergedVec++;
	}
}
void MergeSortRI(std::vector<RelativeIndex>& input,
	int const begin,
	int const end)
	// begin is for left index and end is
	// right index of the sub-vector
	// of input to be sorted */
{
	if (begin >= end)
		return; // Returns recursively

	auto mid = begin + (end - begin) / 2;
	MergeSortRI(input, begin, mid);
	MergeSortRI(input, mid + 1, end);
	MergeRI(input, begin, mid, end);
}
void MergeID(std::vector<RelativeIndex>& input,
	int const left,
	int const mid,
	int const right)
	// Merges two subvectors of input.
	// First vector is input[begin..mid]
	// Second vector is input[mid+1..end]
{
	auto const subVecOne = mid - left + 1;
	auto const subVecTwo = right - mid;

	// Create temp vectors
	std::vector <RelativeIndex> leftVec;
	std::vector <RelativeIndex> rightVec;

	// Copy data to temp vectors leftVec[] and rightVec[]
	for (auto i = 0; i < subVecOne; i++)
		leftVec.push_back(input[left + i]);
	for (auto j = 0; j < subVecTwo; j++)
		rightVec.push_back(input[mid + 1 + j]);

	auto indexOfSubVecOne = 0, // Initial index of first sub-vector
		indexOfSubVecTwo = 0; // Initial index of second sub-vector
	int indexOfMergedVec = left; // Initial index of merged vector

	// Merge the temp vectors back into input[left..right]
	while (indexOfSubVecOne < subVecOne && indexOfSubVecTwo < subVecTwo) {
		if (leftVec[indexOfSubVecOne].doc_id <= rightVec[indexOfSubVecTwo].doc_id) {
			input[indexOfMergedVec] = leftVec[indexOfSubVecOne];
			indexOfSubVecOne++;
		}
		else {
			input[indexOfMergedVec] = rightVec[indexOfSubVecTwo];
			indexOfSubVecTwo++;
		}
		indexOfMergedVec++;
	}
	// Copy the remaining elements of
	// left[], if there are any
	while (indexOfSubVecOne < subVecOne) {
		input[indexOfMergedVec] = leftVec[indexOfSubVecOne];
		indexOfSubVecOne++;
		indexOfMergedVec++;
	}
	// Copy the remaining elements of
	// right[], if there are any
	while (indexOfSubVecTwo < subVecTwo) {
		input[indexOfMergedVec] = rightVec[indexOfSubVecTwo];
		indexOfSubVecTwo++;
		indexOfMergedVec++;
	}
}
void MergeSortID(std::vector<RelativeIndex>& input,
	int const begin,
	int const end)
	// begin is for left index and end is
	// right index of the sub-vector
	// of input to be sorted */
{
	if (begin >= end)
		return; // Returns recursively

	auto mid = begin + (end - begin) / 2;
	MergeSortID(input, begin, mid);
	MergeSortID(input, mid + 1, end);
	MergeID(input, begin, mid, end);
}
std::vector <size_t> Union(std::vector <size_t>& v1,
	std::vector <size_t>& v2) {
	std::vector<size_t> answer_docs;
	std::set_union(v1.begin(),
		v1.end(),
		v2.begin(),
		v2.end(),
		std::back_inserter(answer_docs));
	return answer_docs;
}

void IDSort(std::vector<RelativeIndex>& input) {
	int begin{ 0 };
	float init_rank = input[0].rank;
	for (int i = 0; i < input.size();i++) {
		if (input[i].rank!= init_rank || i== input.size()-1) {
			init_rank = input[i].rank;
			std::cout << "Rank changes!\n";
			//  sorting by id part of the input
			MergeSortID(input, begin, i);
			//  move to next range
			begin = i;
		}
	}
}

void RemoveDuplicate(std::vector<size_t>& v) {
	std::vector<size_t>::iterator itr = v.begin();
	std::unordered_set<size_t> s;
	for (auto curr = v.begin(); curr != v.end(); ++curr)
	{
		if (s.insert(*curr).second) {
			*itr++ = *curr;
		}
	}
	v.erase(itr, v.end());
}

std::vector<RelativeIndex> SingleSearch (std::string line,
	std::map<std::string,
	std::vector<Entry>>
	freq_dictionary/*,
	int resp_lim*/) {
	std::vector<RelativeIndex> relevance;
	// 1. parse request line into words
	std::vector<std::string> unique_words = ParseDocs(line);
	// 2. unique words with max count
	std::vector<FreqMaxCount> fmc;
	for (auto& el : unique_words) {
		//  freq_dictionary contains this word
		if (freq_dictionary.count(el) > 0) {  
			FreqMaxCount temp{ el, MaxCount(freq_dictionary[el]) };
			// no repeats
			bool flag = true;
			for (auto& el : fmc) {
				if (el.word == temp.word) flag = false;
			}
			if (flag) fmc.push_back(temp);
		}
	}
	//  no words found in the freq_dictionary
	if (fmc.size() == 0) return relevance;
	std::vector <size_t> answer_docs;
	//  5. vector of doc_id's of requested words
	for (int i = 0; i < fmc.size(); i++) {
		std::vector<size_t> temp;
		for (auto& el : freq_dictionary[fmc[i].word]) {
			temp.push_back(el.doc_id);
			std::cout << fmc[i].word << " - " << el.doc_id << std::endl;
		}
		//  renew answer docs (id's) on every step
		answer_docs = Union(answer_docs, temp);
	}
	RemoveDuplicate(answer_docs);
	//  7. calculate relevance
	for (auto& El : answer_docs) {  //  answer docs ids
		size_t count{ 0 };
		for (auto& el : fmc) {  //  words & counts
			for (auto& f : freq_dictionary[el.word]) {
				//  search for word count of answer in freq_dict by id
				if (f.doc_id == El) count += f.count;
			}
		}
		relevance.push_back({ El,(float)count });
	}
	/* ~For now vector of Relative Index contains
	absolute relevance. We'll get relative relevance
	on next step in search function~ */
	//  Sort by relevance
	MergeSortRI(relevance, 0, relevance.size() - 1);
	//  Sort by doc_id in equal rank range
	IDSort(relevance);
	//relevance.resize(5);
	return relevance;
}

std::vector<std::vector<RelativeIndex>> SearchServer::search(const
	std::vector<std::string>& queries_input) {
	//  get freq_dictionary from InvertedIndex object
	std::map<std::string, std::vector<Entry>>freq_dictionary =
		GetInvertedIndex().GetFreqDictionary();
	//  for each of queries_inputs make SingleSearch
	std::vector<std::vector<RelativeIndex>>v_relevance;
	for (auto& el : queries_input) {
		v_relevance.push_back(SingleSearch(el,
										   freq_dictionary/*,
										   GetResponseLimit()*/));
	}
	//  find max absolute relevance for each doc_id
	std::vector<size_t> max_relevance;
	for (auto& El : v_relevance) {
		size_t temp{ 0 };
		for (auto& el : El) {
			if (el.rank > temp) {
				temp = el.rank;
			}
		}
		max_relevance.push_back(temp);
	}
	std::cout << "Max relevance is: \n";
	for (auto& el : max_relevance) {
		std::cout << el << std::endl;
	}
	//  7. relative relevance
	std::vector < std::vector <RelativeIndex>> relative_relevance;
	for (int i = 0; i < v_relevance.size();i++) {
		std::vector <RelativeIndex > temp;
		for (auto& el : v_relevance[i]) {
			//  no division by zero
			if (max_relevance[i] != 0) {
				temp.push_back({ el.doc_id,el.rank / (float)max_relevance[i] });
			}			
		}
		relative_relevance.push_back(temp);
	}
	//  8. sorting by relevance
	//for (auto& el : relative_relevance) {
	//	//  no empty vectors
	//	if (el.size() != 0) {
	//		MergeSortRI(el,0, el.size()-1);
	//	}
	//}
	std::cout << "Absolute relevance is: \n";
	for (auto& El : v_relevance) {
		for (auto& el : El) {
			std::cout << "doc.id - " << el.doc_id
				<< " : rank - " << el.rank << std::endl;
		}
		std::cout << std::endl;
	}
	std::cout << "Relative relevance is: \n";
	for (auto& El : relative_relevance) {
		for (auto& el : El) {
			std::cout<<"doc.id - " << el.doc_id
				<<" : rank - "<<el.rank << std::endl;
		}
		std::cout << std::endl;
	}
	return relative_relevance;
}
	std::vector<std::vector<std::pair<int, float>>> ConvertAnswer(
		const std::vector < std::vector <RelativeIndex>> answer) {
		std::vector<std::vector<std::pair<int, float>>> output;
		for (auto& El : answer) {
			std::vector<std::pair<int, float>> temp;
			for (auto& el : El) {
				temp.push_back(std::make_pair(int(el.doc_id), el.rank));
			}
			output.push_back(temp);
		}
		return output;
	}