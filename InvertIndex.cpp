#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <thread>
#include <mutex>
#include "InvertIndex.h"


std::vector <Entry> InvertedIndex::GetWordCount(const std::string& word) {
	return freq_dictionary[word];
}

std::vector<std::string> ParseDocs(std::string line) {
	std::vector <std::string> words;
	std::string token;
	std::istringstream ss(line);
	// derive single words from line
	while (ss >> token) {
		words.push_back(token);
	}
	return words;
}

std::vector<std::string> InvertedIndex::GetDocs() {
	return docs;
}

void InvertedIndex::SetDocs(std::vector<std::string> v) {
	docs = v;
}

size_t Count(std::string word, std::string line) {
	size_t count = 0;
	std::string token;
	std::istringstream ss(line);
	while (ss >> token) {
		//  exclude the occurrence of a token in another word
		if (token == word) ++count;
	}
	return count;
}

void Indexing(size_t text_id, std::vector<std::string> docs,
	std::map<std::string, std::vector<Entry>>& freq_dictionary) {
	std::mutex freq_dict_mutex;
	freq_dict_mutex.lock();

	//  1. fill freq_dictionary with words -> keys & empty vectors -> values
	for (auto& el : ParseDocs(docs[text_id])) {
		//  will fill in later
		std::vector<Entry> blank_vec;
		freq_dictionary[el] = blank_vec;
	}
	//  now freq_dictionary is a collection of unique words from text
	//  2. make invert index for freq_dictionary
	for (size_t i = 0; i < docs.size(); i++) {
		for (auto& El : freq_dictionary) {  
			size_t n = Count(El.first, docs[i]);
			Entry temp{ i,n };
			//  search word found in the document
			if (n > 0) {
				bool flag = true;
				//  if this word has already been searched in the text
				for (auto& el : El.second) {
					if (el.doc_id == i) flag = false;
				}
				if (flag) El.second.push_back({ i,n });
			}
		}
	}
	freq_dict_mutex.unlock();
}

std::map<std::string, std::vector<Entry>> InvertedIndex::GetFreqDictionary() {
	return freq_dictionary;
}

void InvertedIndex::SetFreqDictionary(std::map<std::string, std::vector<Entry>> m) {
	freq_dictionary = m;
}

void InvertedIndex::UpdateDocumentBase(std::vector<std::string> input_docs) {
	// renew docs
	std::vector<std::string>updated_docs;
	if (GetDocs().size() > 0) {
		std::vector<std::string>old_docs = GetDocs();
		// insert new & old docs into updated_docs
		std::merge(input_docs.begin(), input_docs.end(),
			old_docs.begin(), old_docs.end(),
			std::back_inserter(updated_docs));
			// set updated_docs as docs in InvertedIndex object
		SetDocs(updated_docs);
		//for (auto& d : updated_docs) {
		//	std::cout << d << std::endl;
		//}
	}
	else {
		std::cout << "input_docs is empty" << std::endl;
		SetDocs(input_docs);
		updated_docs = input_docs;
	}
	std::map<std::string, std::vector<Entry>> new_freq_dictionary = GetFreqDictionary();
	std::vector<std::thread> vec_thr;
	for (int i = 0; i < updated_docs.size(); i++) {
		// indexing each (i) text in its own thread
		vec_thr.emplace_back(std::thread(Indexing, i, updated_docs, std::ref(new_freq_dictionary)));
		std::cout << " thread " << i << " started" << std::endl;
	}
	//  join threads
	for (auto& t : vec_thr) {
		if (t.joinable()) t.join();
	}
	//  update new_freq_dictionary
	SetFreqDictionary(new_freq_dictionary);
	std::cout<<"Freq_dictionary containing:\n";
	for (auto& El : GetFreqDictionary()) {
		std::cout << El.first << ":\n";
		for (auto& el : El.second) {
			std::cout << "\t" <<
			el.doc_id << " : " << el.count << std::endl;
		}
	}
}