#pragma once

#include <iostream>
#include "InvertIndex.h"
#include "JSON.h"

struct FreqMaxCount {
	std::string word;
	size_t count;
};

struct RelativeIndex {
	size_t doc_id;
	float rank;
	bool operator ==(const RelativeIndex& other) const {
		return (doc_id == other.doc_id && rank == other.rank);
	}
};

size_t MaxCount(std::vector<Entry> freq);
void MergeFMC(std::vector<FreqMaxCount>& input,
	int const left,
	int const mid,
	int const right);
void MergeSortFMC(std::vector<FreqMaxCount>& input,
	int const begin,
	int const end);
void MergeRI(std::vector<RelativeIndex>& input,
	int const left,
	int const mid,
	int const right);
void MergeSortRI(std::vector<RelativeIndex>& input,
	int const begin,
	int const end);

std::vector <size_t> Union(std::vector <size_t> &v1,
	std::vector <size_t> &v2);

void RemoveDuplicate(std::vector<size_t>& v);

std::vector<RelativeIndex> SingleSearch (std::string line,
	std::map<std::string,
	std::vector<Entry>>
	freq_dictionary/*,
	int resp_lim*/);

class SearchServer {
public:
	/**
	* @param idx � ����������� ������ ��������� ������ �� �����
	InvertedIndex,
	*      ����� SearchServer ��� ������ ������� ���� ����������� �
	�������
	*/
	SearchServer(InvertedIndex& idx/*, int rl*/) : _index(idx)/*, _resp_limit(rl)*/{};
	/**
	* ����� ��������� ��������� ��������
	* @param queries_input ��������� ������� ������ �� �����
	requests.json
	* @return ���������� ��������������� ������ ����������� ������� ���
	�������� ��������
	*/
	std::vector<std::vector<RelativeIndex>> search(const
		std::vector<std::string>& queries_input);
	InvertedIndex GetInvertedIndex();
	//int GetResponseLimit();
private:
	InvertedIndex _index;
	//int _resp_limit;
};

std::vector<std::vector<std::pair<int, float>>> ConvertAnswer(
	const std::vector < std::vector <RelativeIndex>> answer);
